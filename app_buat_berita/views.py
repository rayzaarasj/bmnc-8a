from django.shortcuts import render
from django.db import connection
from app_database.models import *
from django.db.utils import IntegrityError
import datetime
from django.http import HttpResponseRedirect

response = {}

# Create your views here.
def buat_berita(request):
    if (request.method == 'POST'):
        url = request.POST['url']
        judul = request.POST['judul']
        topik = request.POST['topik']
        now = datetime.datetime.now()
        f = '%Y-%m-%d %H:%M:%S'
        created_at = now.strftime(f)
        updated_at = created_at
        jumlah_kata = int(request.POST['jumlah_kata'])
        rerata_rating = 0.0

        id_narasumber = request.session.get('id_narasumber', 1)
        narasumber = Narasumber.objects.raw("SELECT * FROM NARASUMBER WHERE id=%s" % (id_narasumber))[0]
        id_universitas = narasumber.id_universitas.id
        tag = request.POST['tag']

        try:
            with connection.cursor() as cursor:
                command = "INSERT INTO BERITA VALUES ('%s','%s','%s','%s','%s','%d','%f','%d')" % (url,judul,topik,created_at,updated_at,jumlah_kata,rerata_rating,id_universitas)
                cursor.execute(command)
                command = "INSERT INTO TAG VALUES ('%s','%s')" % (url,tag)
                cursor.execute(command)
                response['error'] = None
                response['success'] = "Data berhasil disimpan"
        except IntegrityError as e:
            response['error'] = 'URL tidak valid'
            response['success'] = None

    return render(request, 'buat-berita.html', response)

def go_to_profile(request):
    if 'username' in request.session:
        idnarasumber = Users.objects.raw("SELECT * FROM USERS WHERE username='%s'" % request.session.get('username'))[0].id_narasumber.id
        return HttpResponseRedirect('../../profile/' + str(idnarasumber))
