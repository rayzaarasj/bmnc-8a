from django.urls import path
from . import views

app_name = 'app_buat_berita'
urlpatterns = [
    path('', views.buat_berita, name='buat_berita'),
    path('/go-to-profile', views.go_to_profile, name='go_to_profile')
]
