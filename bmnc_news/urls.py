"""bmnc_news URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.views.generic import RedirectView
from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
import app_login.urls as login
import app_registrasi.urls as registrasi
from django.urls import path, include
from app_polling import views as views_polling


urlpatterns = [
    path('admin/', admin.site.urls),
    path('profile/', include('app_profile.urls')),
    path('berita/', include('app_berita.urls')),
    path('polling/', views_polling.index),
    path('buat-berita/', include('app_buat_berita.urls', namespace='app_buat_berita')),
    path('buat-polling-berita/', include('app_buat_polling_berita.urls', namespace='app_buat_polling_berita')),
    path('buat-polling-biasa/', include('app_buat_polling_biasa.urls', namespace='app_buat_polling_biasa')),
    url(r'^login/', include(('app_login.urls','login'), namespace='login')),
    url(r'^registrasi/', include(('app_registrasi.urls','registrasi'), namespace='registrasi')),
    url(r'^$', RedirectView.as_view(url='/login/', permanent=True), name='index'),
]
