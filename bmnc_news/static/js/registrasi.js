var peran = $('.dropdown-item :selected').text();

var vm = new Vue({
	delimiters: ['$(', ')'],
	el: '#app',
	data: {
		alert_success: false,
		alert_danger: false,
		role: peran,
		username: { value: "", valid: true },
		password: { value: "", valid: false },
		nomor_identitas: { value: "", valid: false },
		nama: { value: "", valid: true },
		tempat_lahir: { value: "", valid: true },
		tanggal_lahir: { value: "", valid: false },
		email: { value: "", valid: false },
		nomor_hp: { value: null, valid: true },
		statusId: { value: "", valid: true },
	},
	methods: {
		submitForm: function(e) {
			e.preventDefault();
			var postdata = {}
			var check = this.username.valid && this.password.valid && this.nomor_identitas.valid && this.nama.valid && this.tempat_lahir.valid && this.tanggal_lahir.valid && this.email.valid && this.nomor_hp.valid && this.statusId.valid;
			if(check){
				this.$http.post('/registrasi/',JSON.stringify(this.post_data))
				.then(res => res.json())
				.then(res => {
					console.log(res.status);
					if(res.status == "valid"){
						this.alert_danger = false;
						this.alert_success = true;
						setTimeout(function() {
                            location.href="/login";
                        },3000);
					} else {
						this.alert_danger = true;
						this.alert_success = false;
					}
				})
				.catch(err => console.log(err));
			} else {
				alert('Form is Invalid');
			}
		},
	},
	computed: {
		post_data: function() {
			var form_data = {}
			form_data["role"] = this.role;
			form_data["username"] = this.username.value;
			form_data["password"] = this.password.value;
			form_data["nomor_identitas"] = this.nomor_identitas.value;
			form_data["nama"] = this.nama.value;
			form_data["tempat_lahir"] = this.tempat_lahir.value;
			form_data["tanggal_lahir"] = this.tanggal_lahir.value;
			form_data["email"] = this.email.value;
			form_data["nomor_hp"] = this.nomor_hp.value;
			form_data["statusId"] = this.statusId.value;
			return form_data;
		}
	},
	watch: {
		password: {
			handler: function(oldVal,newVal) {
				if (newVal.value.length >= 8 && /^[a-zA-Z0-9]*$/.test(newVal.value) {
                    newVal.valid = true;
                } else {
                    newVal.valid = false;
                }
            },
            deep: true
        },
		nomor_identitas : {
			handler: function(oldVal, newVal) {
				newVal.forEach(number => {
                    if (/(^([+]?[0-9])+$|^$)/.test(number.value)) {
                        number.valid = true;
                    } else {
                        number.valid = false;
                    }
                });
			},
			deep: true
		},
		tanggal_lahir : {
			handler: function(oldVal, newVal) {
				if (/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(newVal.value) {
					newVal.valid = true;
				} else {
					newVal.valid = false;
				}
			},
			deep: true
		},
		email : {
			handler: function(oldVal, newVal) {
				if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(newVal.value)) {
                    newVal.valid = true;
                } else {
                    newVal.valid = false;
                }
            },
            deep: true
        }
	},
});
	