from django.shortcuts import render
from django.db import connection
from app_database.models import *
from django.db.utils import ProgrammingError
response = {}
# Create your views here.
def buat_polling_berita(request):
    response['error'] = None
    response['success'] = None
    if (request.method=="POST"):
        id_polling = len(list(Polling.objects.raw("SELECT * FROM POLLING"))) + 1
        waktu_mulai = request.POST['waktu_mulai'] + " 00:00:00"
        waktu_selesai = request.POST['waktu_selesai'] + " 23:59:59"
        total_responded = 0

        url_berita = request.POST['url_berita']

        try:
            list(Berita.objects.raw("SELECT * FROM BERITA WHERE url='" + url_berita + "'"))
            with connection.cursor() as cursor:
                command = "INSERT INTO POLLING VALUES ('%d','%s','%s','%d')" % (id_polling,waktu_mulai,waktu_selesai,total_responded)
                cursor.execute(command)
                command = "INSERT INTO POLLING_BERITA VALUES ('%d','%s')" % (id_polling,url_berita)
                cursor.execute(command)
                response['error'] = None
                response['success'] = "Data berhasil disimpan"
        except ProgrammingError as e:
            response['error'] = 'URL Berita tidak valid'
            response['success'] = None

    return render(request, 'buat-polling-berita.html', response)
