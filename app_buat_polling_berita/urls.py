from django.urls import path
from . import views

app_name = 'app_buat_polling_berita'
urlpatterns = [
    path('', views.buat_polling_berita, name='buat_polling_berita')
]
