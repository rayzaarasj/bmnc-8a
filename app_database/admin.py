from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Berita)
admin.site.register(Dosen)
admin.site.register(Honor)
admin.site.register(Komentar)
admin.site.register(Kupon)
admin.site.register(Mahasiswa)
admin.site.register(Narasumber)
admin.site.register(NarasumberBerita)
admin.site.register(Polling)
admin.site.register(PollingBerita)
admin.site.register(PollingBiasa)
admin.site.register(Rating)
admin.site.register(Rekening)
admin.site.register(Respon)
admin.site.register(Responden)
admin.site.register(Riwayat)
admin.site.register(Staf)
admin.site.register(Tag)
admin.site.register(Users)
admin.site.register(Universitas)
