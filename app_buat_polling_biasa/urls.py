from django.urls import path
from . import views

app_name = 'app_buat_polling_biasa'
urlpatterns = [
    path('', views.buat_polling_biasa, name='buat_polling_biasa')
]
