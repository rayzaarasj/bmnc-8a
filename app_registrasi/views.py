from django.shortcuts import render
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import SuspiciousOperation
from django.db import IntegrityError, connection
from app_database.models import Users, Narasumber, Mahasiswa, Dosen, Staf
from random import randrange
import json
import random

USER_REG = ['role','username', 'password','nomor_identitas','nama','tempat_lahir','tanggal_lahir','email','nomor_hp','statusId']
Jurusan = ['Kedokteran', 'Ilmu Komputer', 'Sistem Informasi', 'Teknik Industri', 'Ekonomi', 'Ilmu Politik', 'Hukum']
Posisi = ['Kemahasiswaan', 'Akademis', 'Keuangan', 'Kesehatan', 'Kebersihan']
response = {}

# Create your views here.
@csrf_exempt
@require_http_methods(['GET','POST'])
def index(request):
    if 'username' in request.session:
        return HttpResponseRedirect(reverse('app_login:index'))
    if request.method == 'GET':
        return render(request,'app_registrasi/registrasi.html',response)
    else :
        try:
            data = json.loads(request.body.decode('utf-8'))
            jumlah = len(list(Narasumber.objects.raw("SELECT * FROM Narasumber")))
            print(data['role'])
            with connection.cursor() as cursor:
                if data['role'] == 'mahasiswa' :
                    random_number = random.randint(1,50)
                    command = "INSERT INTO Narasumber VALUES('%d', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%d')" % (jumlah+1, data['nama'], data['email'], data['tempat_lahir'], data['tanggal_lahir'], data['nomor_hp'], 0, 0, random_number)
                    cursor.execute(command)
                    command = "INSERT INTO Mahasiswa VALUES('%d', '%s', '%s')" % (jumlah+1, data['nomor_identitas'], data['statusId'])
                    cursor.execute(command)
                    command = "INSERT INTO Users VALUES('%s', '%s', '%d')" % (data['username'], data['password'], jumlah+1)
                    cursor.execute(command)
                elif data['role'] == 'dosen' :
                    random_index = randrange(0,len(Jurusan)-1)
                    command = "INSERT INTO Narasumber VALUES('%d', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%d')" % (jumlah+1, data['nama'], data['email'], data['tempat_lahir'], data['tanggal_lahir'], data['nomor_hp'], 0, 0, data['statusId'])
                    cursor.execute(command)
                    command = "INSERT INTO Dosen VALUES ('%d', '%s', '%s')" % (jumlah+1, data['nomor_identitas'],  Jurusan[random_index])
                    cursor.execute(command)
                    command = "INSERT INTO Users VALUES('%s', '%s', '%d')" % (data['username'], data['password'], jumlah+1)
                    cursor.execute(command)
                elif data['role'] == 'staf' :
                    random_index = randrange(0,len(Posisi)-1)
                    command = "INSERT INTO Narasumber VALUES('%d', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%d')" % (jumlah+1, data['nama'], data['email'], data['tempat_lahir'], data['tanggal_lahir'], data['nomor_hp'], 0,0, data['statusId'])
                    cursor.execute(command)
                    command = "INSERT INTO Staf VALUES ('%d', '%s', '%s')" % (jumlah+1, data['nomor_identitas'],  Posisi[random_index])
                    cursor.execute(command)
                    command = "INSERT INTO Users VALUES('%s', '%s', '%d')" % (data['username'], data['password'], jumlah+1)
                    cursor.execute(command)
            return JsonResponse({'status':'valid'})
        except IntegrityError:
            return JsonResponse({'status':'invalid'})
        #except :
            #return HttpResponse("<h1>Bad Request</h1>",status=400)
