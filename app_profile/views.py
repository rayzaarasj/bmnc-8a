from django.shortcuts import render
from app_database.models import Narasumber, NarasumberBerita, Berita, Mahasiswa, Dosen, Staf

# Create your views here.

response = {}

def index(request, user_id):
	urlBerita = NarasumberBerita.objects.filter(id_narasumber=user_id).values_list("url_berita", flat=True)
	urlBerita1 = NarasumberBerita.objects.raw('select url_berita from narasumber_berita where id_narasumber=%d', [int(user_id)])
	berita = Berita.objects.filter(pk__in=urlBerita)

	profil = Narasumber.objects.get(id=user_id)
	profil1 = Narasumber.objects.raw('select * from narasumber where id=%s' % user_id)

	mahasiswa = Mahasiswa.objects.filter(id_narasumber=user_id)
	dosen = Dosen.objects.filter(id_narasumber=user_id)
	staf = Staf.objects.filter(id_narasumber=user_id)

	try:
		mahasiswa1 = Mahasiswa.objects.raw("select * from mahasiswa where id_narasumber=%s" % user_id)[0]
	except IndexError:
		mahasiswa1 = False

	try:
		dosen1 = Dosen.objects.raw('select * from dosen where id_narasumber=%s' % user_id)[0]
	except IndexError:
		dosen1 = False

	try:
		staf1 = Staf.objects.raw('select * from staf where id_narasumber=%s' % user_id)[0]
	except IndexError:
		staf1 = False

	response['mahasiswa1'] = mahasiswa1
	response['dosen1'] = dosen1
	response['staf1'] = staf1

	response["berita"] = berita
	response["profil"] = profil
	response["mahasiswa"] = mahasiswa
	response["dosen"] = dosen
	response["staf"] = staf
	html = 'app_profile/profile.html'
	return render(request, html, response)
