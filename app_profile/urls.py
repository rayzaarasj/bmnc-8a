from django.conf.urls import url
from django.urls import path, re_path
from . import views


urlpatterns = [
    #path('<user_id>', views.index),
    re_path(r'^(?P<user_id>[0-9]+)/$', views.index),
]
