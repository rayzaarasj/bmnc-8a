from .views import index, login, logout
from django.conf.urls import url

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^login', login, name='login'),
    url(r'^logout', logout, name='logout'),
]
