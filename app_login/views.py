from django.shortcuts import render

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from app_database.models import Users

response = {}

# Create your views here.

def index(request):
    content = {}
    if 'username' in request.session:
        idnarasumber = Users.objects.raw("SELECT * FROM USERS WHERE username='%s'" % request.session.get('username'))[0].id_narasumber.id
        return HttpResponseRedirect('../profile/' + str(idnarasumber))
    elif 'invalid' in request.session:
        content['alert']=request.session['invalid']
        del request.session['invalid']
    else:
        content['alert']=''
    return render(request, 'app_login/login.html', content)

def login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        narasumber = Users.objects.raw('SELECT * FROM Users WHERE username = %s', [username])

        if (username == '' or password == ''):
            request.session['invalid'] = 'Lengkapi Informasi Login'
        elif not narasumber:
            request.session['invalid'] = 'Informasi Login Tidak Valid'
        else:
            try :
                if(narasumber[0].password!=password):
                    request.session['invalid'] = 'Informasi Login Tidak Valid'
                else :
                    request.session['username'] = username
                    id = narasumber[0].id_narasumber
                    request.session['id_narasumber'] = id.id
            except IndexError:
                request.session['invalid'] = 'Username Tidak Terdaftar'
    return HttpResponseRedirect(reverse('login:index'))

def logout(request):
    if 'username' in request.session:
        request.session.flush()
    return HttpResponseRedirect(reverse('login:index'))
