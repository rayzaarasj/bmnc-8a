from django.apps import AppConfig


class AppPollingConfig(AppConfig):
    name = 'app_polling'
