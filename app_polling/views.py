from django.shortcuts import render
from app_database.models import Polling, PollingBerita, PollingBiasa, Respon

# Create your views here.
response = {}

def index(request):
	pollingBiasa = PollingBiasa.objects.raw('select * from polling_biasa')
	pollingBerita = PollingBerita.objects.raw('select * from polling_berita')
	response['pollingBiasa'] = pollingBiasa
	response['pollingBerita'] = pollingBerita
	html = 'app_polling/polling.html'
	return render(request, html, response)
